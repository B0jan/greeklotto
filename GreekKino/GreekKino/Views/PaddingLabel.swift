//
//  PaddingLabel.swift
//  GreekKino
//
//  Created by Administrator on 21/10/2020.
//

import Foundation
import UIKit






    
    
    
    

//MARK: - Label padding
class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 1.0
    @IBInspectable var bottomInset: CGFloat = 1.0
    @IBInspectable var leftInset: CGFloat = 3.0
    @IBInspectable var rightInset: CGFloat = 3.0
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        textColor = .white
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        textColor = .white
        clipsToBounds = true
        
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}




class InfoLabel:UILabel{
    
    
    //    MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.textColor = .white
        self.font = UIFont.systemFont(ofSize: 12)
        self.sizeToFit ()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

    
