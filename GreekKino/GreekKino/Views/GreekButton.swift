//
//  GreekButton.swift
//  GreekKino
//
//  Created by Administrator on 21/10/2020.
//

import Foundation
import UIKit



class GreekButton:UIButton{
    //    MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setTitleColor(.white, for: .normal)
        self.setTitleColor(.greekOldGoldDark, for: .selected)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        self.backgroundColor = .black
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.greekOldGoldDark.cgColor
        self.roundCorners(5)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
