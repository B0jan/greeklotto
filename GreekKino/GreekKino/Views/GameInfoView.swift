//
//  GameInfoView.swift
//  GreekKino
//
//  Created by Administrator on 19/10/2020.
//

import UIKit

class GameInfoView: UIView {
    
    //    MARK: - Properties
    private var timeManager:TimeManager?
    
    //    MARK: - UIElements
    private lazy var infoLabel:InfoLabel = {
        var label = InfoLabel()
        label.text = "Info"
        return label
    }()
    
    private lazy var roundLabel:InfoLabel = {
        var label = InfoLabel()
        return label
    }()
    
    
    private lazy var startTimeLabel:InfoLabel = {
        var label = InfoLabel()
        return label
    }()
    
    
    private lazy var remainingTime:InfoLabel = {
        var label = InfoLabel()
        return label
    }()
    
    private lazy var numbersSelected:UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20)
        label.sizeToFit ()
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .greekGray
        label.roundCorners(15)
        return label
    }()
    
    
    //    MARK: - Life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.translatesAutoresizingMaskIntoConstraints = false
        setupUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //    MARK: - Actions
    func loadData(from:KinoGame){
        let date = from.drawTime!.toDate()
        let time = date.stringFormattedDate(with: "HH:mm")
        startTimeLabel.text = "Vreme izvlačenja:\(time)"
        roundLabel.text = "Kolo:\(from.drawID!)"
        numbersSelected.text = "0"
        
        if TimeManager.expiredTimes.contains(from.drawTime ?? 0){
            remainingTime.textColor = .red
            remainingTime.text = "0m 0s"
            return
        }
        
        if TimeManager.timeMap[from.drawTime ?? 0] == nil{
            timeManager = TimeManager(with: from)
        } else  {
            timeManager = TimeManager.timeMap[from.drawTime ?? 0]
        }
        
        timeManager?.timeUpdated = { [weak self] timeLeft, time, state in
            switch state {
            case .counting:
                self?.remainingTime.textColor = .white
            case .closing:
                self?.remainingTime.textColor = .red
            case .ended:
                self?.remainingTime.textColor = .red
            }
            self?.remainingTime.text = "Preostalo za uplatu: \(timeLeft)"
        }
        
       
    }
    
    func alertNumber(){
        numbersSelected.jitterAlert()
    }
    
    
    func updateNumbersCount(with num:Int){
        numbersSelected.text = num.toString()
        if num == 20 {
            numbersSelected.backgroundColor = .red
            return
        }
        numbersSelected.backgroundColor = .greekGray
    }
    
    //    MARK: - Protocol functions
    
    
    //    MARK: - Helpers
    private func setupUI(){
        eAddSubViews(infoLabel,roundLabel,startTimeLabel,remainingTime,numbersSelected)
        self.backgroundColor = .greekdarkGray
        
        NSLayoutConstraint.activate([
            infoLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            infoLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            
            startTimeLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5),
            startTimeLabel.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 2),
            
            roundLabel.topAnchor.constraint(equalTo: startTimeLabel.bottomAnchor, constant: 5),
            roundLabel.leadingAnchor.constraint(equalTo: startTimeLabel.leadingAnchor),
            
            remainingTime.centerYAnchor.constraint(equalTo: roundLabel.centerYAnchor),
            remainingTime.leadingAnchor.constraint(equalTo: startTimeLabel.trailingAnchor, constant: 5),
            
            numbersSelected.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
            numbersSelected.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            numbersSelected.widthAnchor.constraint(equalToConstant: 30),
            numbersSelected.heightAnchor.constraint(equalToConstant: 30)
            
        ])
    }
    
    
    
    
}
