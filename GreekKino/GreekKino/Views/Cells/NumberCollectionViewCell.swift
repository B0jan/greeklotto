//
//  NumberCollectionViewCell.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import UIKit

class NumberCollectionViewCell: UICollectionViewCell {
    
    //    MARK: - Properties
    var isSelectedD = false
    
    //    MARK: - UIElements
    private lazy var selectionBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.random.cgColor
        return view
    }()
    
    
    private lazy var numberLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.textColor = .white
        return label
    }()
    
    
    //    MARK: - Life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.eAddSubViews(selectionBackgroundView,numberLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        selectionBackgroundView.backgroundColor = .black
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let size = traitCollection.horizontalSizeClass == .compact ?
            min(min(frame.width, frame.height) - 10, 60) : 45
        
        NSLayoutConstraint.activate([
            numberLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            numberLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            selectionBackgroundView.centerYAnchor
                .constraint(equalTo: numberLabel.centerYAnchor),
            selectionBackgroundView.centerXAnchor
                .constraint(equalTo: numberLabel.centerXAnchor),
            selectionBackgroundView.widthAnchor.constraint(equalToConstant: size),
            selectionBackgroundView.heightAnchor
                .constraint(equalTo: selectionBackgroundView.widthAnchor)
        ])
        
        selectionBackgroundView.layer.cornerRadius = size / 2
    }
    
    
    func configure(num:Int,isSelected:Bool){
        self.isSelectedD = isSelected
        self.isSelectedD ? (selectionBackgroundView.backgroundColor = .greekOldGoldLight) : (selectionBackgroundView.backgroundColor = .black)
        numberLabel.text = String(num)
    }
    
    
    //    MARK: - Actions
    func setSelected(){
        isSelectedD = !isSelectedD
        if isSelectedD {
            selectionBackgroundView.backgroundColor = .greekOldGoldLight
        } else {
            selectionBackgroundView.backgroundColor = .black
        }
    }
    
    //    MARK: - Protocol functions
    
    
    
    //    MARK: - Helpers
    
    
    
}
