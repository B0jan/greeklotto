//
//  GameCell.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import UIKit

class GameCell:UITableViewCell{
    
    //    MARK: - Properties
   private var game:KinoGame?
   private var timeManager:TimeManager?
    
    
    //    MARK: - UIElements
    private lazy var timeLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textColor = .white
        lbl.sizeToFit()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 0
        return lbl
    }()
    
    private lazy var countDownLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.textColor = .white
        lbl.sizeToFit()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 1
        return lbl
    }()
    
    
   

    //    MARK: - Life cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.greekOldGoldDark
        self.selectedBackgroundView = bgColorView
        self.contentView.eAddSubViews(countDownLabel,timeLabel)
        self.backgroundColor = .greekOldGoldDark
        self.contentView.layer.cornerRadius = 10
        self.contentView.clipsToBounds = true
        self.contentView.backgroundColor = .black

        NSLayoutConstraint.activate([
            timeLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            timeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            timeLabel.heightAnchor.constraint(equalToConstant: 40),
            
            countDownLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            countDownLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
            countDownLabel.heightAnchor.constraint(equalToConstant: 40),
        ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        timeLabel.text = nil
        countDownLabel.text = nil
        timeManager = nil
    }
    
    
    
    //    MARK: - Actions
    
    
    //    MARK: - Protocol functions
    
    
    
    //    MARK: - Helpers
    func configure(with game:KinoGame){
        self.game = game
        let date = game.drawTime!.toDate()
        let time = date.stringFormattedDate(with: "HH:mm")
        timeLabel.text = time
        timerInit(game)
        timerObserve(game)
//        if TimeManager.expiredTimes.contains(game.drawTime!){
//            countDownLabel.text = "0m 0s"
//            return
//        }
        
        let info = timeManager?.getTimeleft()
        guard let infoSafe = info else {
            countDownLabel.textColor = .red
            countDownLabel.text = "0m 0s"
            return
        }
        switch infoSafe.1 {
        case .counting:
            countDownLabel.textColor = .white
        case .closing:
            countDownLabel.textColor = .red
        case .ended:
            countDownLabel.textColor = .red
        }
        countDownLabel.text = info?.0
        
    }
    
    
    private func timerInit(_ game: KinoGame ) {
        if TimeManager.expiredTimes.contains(game.drawTime ?? 0){
            return
        }
        if TimeManager.timeMap[game.drawTime ?? 0] == nil {
            timeManager = TimeManager(with: game)
        } else  {
            timeManager = TimeManager.timeMap[game.drawTime ?? 0]
        }
    }
    
    
    private func timerObserve(_ game:KinoGame){
        timeManager?.timeUpdated = { [weak self] timeLeft, time, state in
            if self?.game?.drawTime == time{
                
                switch state {
                case .counting:
                    self?.countDownLabel.textColor = .white
                case .closing:
                    self?.countDownLabel.textColor = .red
                case .ended:
                    self?.countDownLabel.textColor = .red
                }
                self?.countDownLabel.text = timeLeft
        }
        
        }
    }
    
}
