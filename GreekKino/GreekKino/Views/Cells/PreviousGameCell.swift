//
//  PreviousGameCell.swift
//  GreekKino
//
//  Created by Administrator on 19/10/2020.
//

import UIKit


class PreviousGameCell: UITableViewCell {

    
    //    MARK: - Properties
    private var drawnNumbers:[Int] = []
    
    //    MARK: - UIElements
    private lazy var label:PaddingLabel = {
        var lbl = PaddingLabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.roundCorners(10)
        lbl.leftInset = 5
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.backgroundColor = .greekOldGoldDark
        lbl.textColor = .white
        return lbl
    }()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 50, height: 50)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.isScrollEnabled = false
        collectionView.isUserInteractionEnabled = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    
    
    
    //    MARK: - Life cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        self.backgroundColor = .black
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //    MARK: - Actions
    
    
    //    MARK: - Protocol functions
    
    
    //    MARK: - Helpers
    private func setupUI(){
        self.backgroundColor = .black
        collectionView.register(NumberCollectionViewCell.self, forCellWithReuseIdentifier: Constants.Cell.numCell)
        collectionView.dataSource = self
        
        self.contentView.eAddSubViews(label,collectionView)

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            label.heightAnchor.constraint(equalToConstant: 40),
            
            collectionView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -10),
            collectionView.topAnchor.constraint(equalTo: label.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10),
            collectionView.heightAnchor.constraint(equalToConstant: 150),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        
        ])
    }
    
    
    func configure(with game:KinoGame){
        let date = game.drawTime!.toDate()
        let time = date.stringFormattedDate(with: "MMM d, h:mm a")
        label.text = "Vreme izvlačenja:\(time) | Kolo:\(game.drawID!)"
        guard let nums = game.winningNumbers?.list else {assertionFailure("Somethhing wrong with server data")/*notify server that list is missing */;return }
            drawnNumbers = nums
    }
    
}


extension PreviousGameCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cell.numCell, for: indexPath) as! NumberCollectionViewCell
        cell.configure(num: drawnNumbers[indexPath.item], isSelected: false)
        return cell
    }
}

