//
//  OddsCell.swift
//  GreekKino
//
//  Created by Administrator on 20/10/2020.
//

import UIKit

class OddsCell: UICollectionViewCell {
    
    //    MARK: - Properties
    
    //    MARK: - UIElements
    private lazy var rbLabel:UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .white
        label.contentMode = .center
        label.textAlignment = .center
        return label
    }()
    
    private lazy var oddsLabel:UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .white
        label.contentMode = .center
        label.textAlignment = .center
        return label
    }()
    
    private var stackView:UIStackView = {
        var stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    
    //    MARK: - Life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        drawLineFromPoint(start: CGPoint(x: 0, y: 30), toPoint: CGPoint(x: 50, y: 30), ofColor: .white, lineWidth: 1)
    }
    
    //    MARK: - Actions
    
    //    MARK: - Protocol functions
    
    //    MARK: - Helpers
    func configure(with num:Int){
        num.isMultiple(of: 2) ? (self.contentView.backgroundColor = .greekdarkGray) : (self.contentView.backgroundColor = .greeklightGray)
        rbLabel.text = num.toString()
        oddsLabel.text = (num * 15).toString() //just for presentation purposes, real numbers are differente
    }
    
    private func setupUI(){
        stackView.addArrangedSubview(rbLabel)
        stackView.addArrangedSubview(oddsLabel)
        self.contentView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        ])
        
    }
    
    
    
    
}
