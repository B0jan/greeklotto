//
//  OddsView.swift
//  GreekKino
//
//  Created by Administrator on 19/10/2020.
//

import UIKit

class OddsView: UIView {
    
    //    MARK: - Properties
    
    
    //    MARK: - UIElements
    private lazy var rbLabel:UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "R.B"
        label.textColor = .white
        label.backgroundColor = .greekdarkGray
        label.font = UIFont.systemFont(ofSize: 12)
        label.contentMode = .center
        label.textAlignment = .center
        return label
    }()
    
    private lazy var oddsLabel:UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Kvota"
        label.textColor = .white
        label.backgroundColor = .greekdarkGray
        label.font = UIFont.systemFont(ofSize: 12)
        label.contentMode = .center
        label.textAlignment = .center
        return label
    }()
    
    private lazy var oddsCollectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 50, height: 60)
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(OddsCell.self, forCellWithReuseIdentifier: Constants.Cell.oddsCell)
        collectionView.dataSource = self
        collectionView.isScrollEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    
    //    MARK: - Life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //    MARK: - Actions
    
    
    
    //    MARK: - Protocol functions
    
    
    
    //    MARK: - Helpers
    private func setupUI(){
        self.translatesAutoresizingMaskIntoConstraints = false
        eAddSubViews(rbLabel,oddsLabel,oddsCollectionView)
        NSLayoutConstraint.activate([
            rbLabel.topAnchor.constraint(equalTo: self.topAnchor),
            rbLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            rbLabel.heightAnchor.constraint(equalToConstant: 30),
            rbLabel.widthAnchor.constraint(equalToConstant: 50),
            
            oddsLabel.topAnchor.constraint(equalTo: rbLabel.bottomAnchor),
            oddsLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            oddsLabel.heightAnchor.constraint(equalToConstant: 30),
            oddsLabel.widthAnchor.constraint(equalToConstant: 50),
            
            oddsCollectionView.leadingAnchor.constraint(equalTo: rbLabel.trailingAnchor),
            oddsCollectionView.topAnchor.constraint(equalTo: self.topAnchor),
            oddsCollectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            oddsCollectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
            
            
        ])
        
    }
}


extension OddsView:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cell.oddsCell, for: indexPath) as! OddsCell
        cell.configure(with:indexPath.item+1)
        return cell
    }
    
    
    
    
}
