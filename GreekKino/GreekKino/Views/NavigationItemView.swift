//
//  NavigationItemView.swift
//  GreekKino
//
//  Created by Administrator on 21/10/2020.
//

import Foundation
import UIKit


class NavigationItemView:UIView{
    
    private lazy var imageView: UIImageView = {
        var imgV = UIImageView()
        let image =  UIImage(named: "nav_icon")
        imgV.translatesAutoresizingMaskIntoConstraints = false
        image?.withAlignmentRectInsets(UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        imgV.image = image
        return imgV
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        /// Preparing UIView
        setupUI()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    fileprivate func setupUI() {
        addSubview(imageView)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 50),
            imageView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }

}
