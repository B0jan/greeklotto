//
//  Constants.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import Foundation


struct Constants{

    struct Cell {
        static let gameCell = "gameCell"
        static let numCell = "numCell"
        static let historyCell = "historyCell"
        static let oddsCell = "oddsCell"
    }
    
}
