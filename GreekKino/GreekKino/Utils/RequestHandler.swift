//
//  RequestsHandler.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import Foundation

enum RequestsState{
    case inProgres, done, failed, noActiveRequest
}


class RequestHandler {
    
    static var requestsState:RequestsState = .noActiveRequest
    func getNewGames(completion: @escaping ([KinoGame]?,APIError?)->()){
        
        RequestHandler.requestsState = .inProgres
        let request = APIRequest(method: .get, path: getPath(newGames: true))
        
        APIClient().perform(request) { (result) in
            switch result{
            case .success(let resp):
                
                if resp.statusCode == 200{
                    RequestHandler.requestsState = .done
                    //        self.printResponse(data: resp.body!)
                    do {
                        if let response = try resp.decode(to: [KinoGame].self) {
                            DispatchQueue.main.async {
                                completion(response.body, nil)
                            }
                        }
                    } catch {
                        DispatchQueue.main.async {
                            completion(nil,APIError.decodingFailure)
                        }
                        //notify  about package decode problem(Slack,Firebase...)
                    }
                }
                
            case .failure(let err):
                RequestHandler.requestsState = .failed
                switch err{
                case .invalidURL:
                    printInDebug("INVALID URL")
                    DispatchQueue.main.async {
                        completion(nil,err)
                    }
                case .requestFailed:
                    printInDebug("REQUEST FAILED")
                    DispatchQueue.main.async {
                        completion(nil,err)
                    }
                case .decodingFailure:
                    printInDebug("DECODING FAILURE")
                    DispatchQueue.main.async {
                        completion(nil,err)
                    }
                }
            }
            
        }
    }
    
    
    
    func getOldGames(completion: @escaping (GameHistory?,APIError?)->()){
        
        RequestHandler.requestsState = .inProgres
        let request = APIRequest(method: .get, path: getPath(newGames: false))
        
        APIClient().perform(request) { (result) in
            switch result{
            case .success(let resp):
                
                if resp.statusCode == 200{
                    RequestHandler.requestsState = .done
                    //  self.printResponse(data: resp.body!)
                    do {
                        if let response = try resp.decode(to: GameHistory.self) {
                            DispatchQueue.main.async {
                                completion(response.body,nil)
                            }
                        }
                    } catch {
                        DispatchQueue.main.async {
                            completion(nil,APIError.decodingFailure)
                        }
                        //notify  about package decode problem(Slack,Firebase...)
                    }
                }
            case .failure(let err):
                RequestHandler.requestsState = .failed
                switch err{
                case .invalidURL:
                    printInDebug("INVALID URL")
                    DispatchQueue.main.async {
                        completion(nil,err)
                    }
                case .requestFailed:
                    printInDebug("REQUEST FAILED")
                    DispatchQueue.main.async {
                        completion(nil,err)
                    }
                case .decodingFailure:
                    printInDebug("DECODING FAILURE")
                    DispatchQueue.main.async {
                        completion(nil,err)
                    }
                }
            }
            
        }
    }
    
    @discardableResult
    private func printResponse(data:Data) -> String{
        let jsonString = String(data: data, encoding: String.Encoding.utf8) ?? "Data could not be printed"
        printInDebug(jsonString.replacingOccurrences(of: "\\", with: ""))
        return jsonString
    }
    
    private func getPath(newGames:Bool) -> String{
        if newGames{
            return "1100/upcoming/20"
        }
        return "1100/draw-date/2020-10-19/2020-10-19"
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
