//
//  UIExtension.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import UIKit


extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func delete(object: Element) {
        if let index = firstIndex(of: object) {
            remove(at: index)
        }
    }
    
}

extension UIColor {
    
    static var webViewBackgroundColor: UIColor {
        return UIColor(red: 3/255, green: 26/255, blue: 40/255, alpha: 1)
    }
    
    static var greekGray: UIColor {
        return UIColor(red: 74/255, green: 68/255, blue: 66/255, alpha: 1)
    }
    
    static var greekdarkGray: UIColor {
        return UIColor(red: 55/255, green: 54/255, blue: 52/255, alpha: 1)
    }
    
    static var greeklightGray: UIColor {
        return UIColor(red: 121/255, green: 120/255, blue: 119/255, alpha: 1)
    }
    
    static var greekOldGoldLight: UIColor {
        return UIColor(red: 207/255, green: 181/255, blue: 59/255, alpha: 1)
    }
    
    static var greekOldGoldDark: UIColor {
        return UIColor(red: 152/255, green: 132/255, blue: 37/255, alpha: 1)
    }
    
    static var random: UIColor {
           let r:CGFloat  = .random(in: 0...1)
           let g:CGFloat  = .random(in: 0...1)
           let b:CGFloat  = .random(in: 0...1)
           return UIColor(red: r, green: g, blue: b, alpha: 1)
       }
}


func printInDebug(_ item: Any){
    #if DEBUG
    print(":::",Date().consoleTime(with: "HH:mm:ss"),":::", " ", item)
    #endif
}


extension Int{
    func toString() -> String{
        return "\(self)"
    }
}


extension Int64{
    func toDate() -> Date {
        let myDate = Date(timeIntervalSince1970:Double(self)/1000)
        return myDate
    }

}


extension Date {
    
    init(milliseconds:Int64) {
          self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
            self.addTimeInterval(TimeInterval(Double(milliseconds % 1000) / 1000 ))
      }
    
    func stringFormattedDate(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
       // dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self)
    }
    
    func consoleTime(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: self)
    }
}


//MARK: - UIViewController
extension UIViewController{
    
    func eActivityStartAnimating(activityColor: UIColor = UIColor.greekOldGoldDark, backgroundColor: UIColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.8)) {
        let backgroundView = UIView()
        backgroundView.frame = CGRect.init(x: UIScreen.main.bounds.midX - 27.5, y: UIScreen.main.bounds.midY - 77.5, width: 50, height:50)
        backgroundView.backgroundColor = .clear
        backgroundView.clipsToBounds = true
        backgroundView.tag = 475647
        
        var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center.x = activityIndicator.frame.width / 2 + 1
        activityIndicator.center.y = activityIndicator.frame.height / 2 + 1
        activityIndicator.hidesWhenStopped = true
        if #available(iOS 13.0, *) {
            activityIndicator.style = UIActivityIndicatorView.Style.large
        } else {
          
            // Fallback on earlier versions
        }
        activityIndicator.color = activityColor
        activityIndicator.startAnimating()
        //        self.isUserInteractionEnabled = false
        
        self.view.addSubview(backgroundView)
        
        backgroundView.addSubview(activityIndicator)
    }
    
    
    func eActivityStopAnimating() {
        if let background = view.viewWithTag(475647){
            background.removeFromSuperview()
            
        }
        //        self.isUserInteractionEnabled = true
    }
    

    func ePushController(_ viewController:UIViewController){
        self.navigationController!.pushViewController(viewController, animated: true)
    }


}


extension UIView {
    
    func drawLineFromPoint(start : CGPoint, toPoint end:CGPoint, ofColor lineColor: UIColor,lineWidth:CGFloat) {
        
        //design the path
        let path = UIBezierPath()
        path.move(to: start)
        path.addLine(to: end)
        
        //design path in layer
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = lineColor.cgColor
        shapeLayer.lineWidth = lineWidth
        
        self.layer.addSublayer(shapeLayer)
    }
    
    ///Adding subViews trough array insted of one by one
    func eAddSubViews(_ views:UIView...){
        views.forEach{addSubview($0)}
    }
        
        func roundOff() {
            //        layoutIfNeeded()
            layer.cornerRadius = frame.size.height / 2
            clipsToBounds = true
        }
        
        func roundCorners(_ amount: CGFloat) {
            //        layoutIfNeeded()
            layer.cornerRadius = amount
            clipsToBounds = true
        }
        
        func removeSubViews(){
            for sub in self.subviews {
                sub.removeFromSuperview()
            }
        }
        
        
        /// jittering fields
    func jitterAlert() {
            
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.05
            animation.repeatCount = 5
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint.init(x: self.center.x - 5.0, y: self.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint.init(x: self.center.x + 5.0, y: self.center.y))
            layer.add(animation, forKey: "position")
            
        }
    
    
}

extension UISegmentedControl {
func setSegmentStyle() {
    setBackgroundImage(imageWithColor(color: .greekOldGoldDark), for: .normal, barMetrics: .default)
    setBackgroundImage(imageWithColor(color: .greekOldGoldLight), for: .selected, barMetrics: .default)
    setDividerImage(imageWithColor(color: UIColor.clear), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
   }

    
    private func imageWithColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width:  1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor);
        context!.fill(rect);
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return image!
    }
}
