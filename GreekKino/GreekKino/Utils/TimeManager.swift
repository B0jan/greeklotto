//
//  TimeManager.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import Foundation


enum GameState{
    case counting, closing, ended
}

class TimeManager{
    
    //    MARK: - Properties
    var timer:Timer!
    var drawTime:Int64!
    
    var timeUpdated:((_ newTime:String,_ drawId: Int64?,_ state:GameState)->())?
    
    
    static var timeMap:[Int64: TimeManager] = [:]{
        willSet{
         //   printInDebug("ℹ️ℹ️ℹ️ WILL ADD \(newValue)")
        }
        didSet{
        //    printInDebug("ℹ️ℹ️ℹ️ MAP HAS \(TimeManager.timeMap.count) ELEMENTS")
        }
    }
    
    static var expiredTimes:[Int64] = []{
        didSet{
       //     printInDebug("ℹ️ℹ️ℹ️ TIME UP FOR \(TimeManager.expiredTimes.last) ELEMENT")
        }
    }
    
    //    MARK: - Life cycle
    init(with:KinoGame) {
        TimeManager.timeMap[with.drawTime!] = self
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTimee), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        drawTime = with.drawTime
    }
    
    
    //    MARK: - Helpers
    @objc func updateTimee() {
        let infoForCell = getTimeleft()
        
        self.timeUpdated!(infoForCell.0,self.drawTime, infoForCell.1)
        
    }
    
    func getTimeleft() -> (String,GameState) {
        let userCalendar = Calendar.current
        let date = Date()
        let components = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: date)
        let currentDate = userCalendar.date(from: components)!
        
        let eventD = drawTime.toDate()
        let eventComponents = userCalendar.dateComponents([.hour, .minute, .month, .year, .day, .second], from: eventD)
        let eventDate = userCalendar.date(from: eventComponents)!
        
        let timeLeft = userCalendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: eventDate)
        
        var textToReturn:String = "0m 0s"
        if timeLeft.second! <= 0 && timeLeft.minute! <= 0 && timeLeft.hour! <= 0 {
            let tim = TimeManager.timeMap[drawTime]
            tim!.timer.invalidate()
            TimeManager.timeMap.removeValue(forKey: drawTime)
            if !TimeManager.expiredTimes.contains(drawTime){
            TimeManager.expiredTimes.append(drawTime)
            } else {
                assertionFailure("NOT SUPPOSE TO HAPEN")
            }
            return (textToReturn,.ended)
        }
        
        if timeLeft.hour != 0 {
            textToReturn = "\(timeLeft.hour!)h \(timeLeft.minute!)m \(timeLeft.second!)s"
        } else {
            textToReturn = "\(timeLeft.minute!)m \(timeLeft.second!)s"
        }
        return (textToReturn,((timeLeft.second! < 15 && timeLeft.minute! == 0  && timeLeft.hour! == 0) ? .closing : .counting))
    }
    

    
}
