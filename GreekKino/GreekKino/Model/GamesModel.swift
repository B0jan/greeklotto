//
//  GamesModel.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import Foundation

struct GameHistory:Codable{
    let content:[KinoGame]?
}

struct KinoGame: Codable,Equatable {
    static func == (lhs: KinoGame, rhs: KinoGame) -> Bool {
        return lhs.drawID == rhs.drawID
    }
    
    let gameID, drawID, drawTime: Int64?
    let status:String
    let winningNumbers: WinningNumbers?
    
    enum CodingKeys: String, CodingKey {
        case gameID = "gameId"
        case drawID = "drawId"
        case drawTime, status, winningNumbers
    }
    
    
    struct WinningNumbers: Codable {
        let list: [Int]?
    }
}
