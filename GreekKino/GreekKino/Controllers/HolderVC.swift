//
//  HolderVC.swift
//  GreekKino
//
//  Created by Administrator on 19/10/2020.
//

import Foundation
import UIKit



class MainVC:UIViewController{
   
    
    //    MARK: - Properties
    private enum Controlers {
        case history,game,stream
    }
    
    private var historyVC:GameHistoryVC?
    private var streamVC:StreamVC?
    private var gameVC:GameVC?
    private var selectedVC:Controlers = .game
    var kinoGame:KinoGame!
    
    
    //    MARK: - UIElements
    private lazy var viewHolder:UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        return view
    }()
    
    private lazy var segmentedPicker:UISegmentedControl = {
        let titles = ["Talon","Izvlačenje uživo","Rezultati izvlačenja"]
        let segmentedControl = UISegmentedControl(items: titles)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.setSegmentStyle()
        let font = UIFont.systemFont(ofSize: 11)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.white],
                                                for: .normal)
        segmentedControl.addTarget(self, action: #selector(segmentAction(_:)), for: .valueChanged)
        segmentedControl.selectedSegmentIndex = 0
        
        return segmentedControl
    }()
    


 
    
    //    MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        present(viewControler: .game)
    }
    
    
    //    MARK: - Actions
    @objc func segmentAction(_ segmentedControl: UISegmentedControl) {
         switch (segmentedControl.selectedSegmentIndex) {
         case 0:
             talonTapped()
         case 1:
            liveTapped()
         case 2:
            historyTapped()
         default:
             break
         }
     }
    
     private func talonTapped(){
        present(viewControler: .game)
    }
    
     private func historyTapped(){
        present(viewControler: .history)
    }
    
    private func liveTapped(){
        present(viewControler: .stream)
    }
    

    //    MARK: - Protocol functions
    
    
    
    //    MARK: - Helpers
    private func setupUI(){
        let navView = NavigationItemView()
        navigationItem.titleView = navView
        view.backgroundColor = .black
        view.eAddSubViews(segmentedPicker,viewHolder)
        
        
        NSLayoutConstraint.activate([
            segmentedPicker.topAnchor.constraint(equalTo: view.topAnchor,constant: 10),
            segmentedPicker.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 5),
            segmentedPicker.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -5),
            segmentedPicker.heightAnchor.constraint(equalToConstant: 50),
            viewHolder.topAnchor.constraint(equalTo: segmentedPicker.bottomAnchor,constant: 5),
            viewHolder.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            viewHolder.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            viewHolder.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func present(viewControler:Controlers){
        
        switch viewControler {
        case .game:
            if let _ = gameVC{
               add(asChildViewController: gameVC!)
            } else {
                gameVC = GameVC()
                gameVC?.kinoGame = kinoGame
                add(asChildViewController: gameVC!)
            }
            selectedVC = .game
        case .history:
            if let _ = historyVC{
                add(asChildViewController: historyVC!)
            } else {
                historyVC = GameHistoryVC()
                add(asChildViewController: historyVC!)
            }
            selectedVC = .history
        case .stream:
            if let _ = streamVC{
                add(asChildViewController: streamVC!)
            } else {
                streamVC = StreamVC()
                add(asChildViewController: streamVC!)
            }
            selectedVC = .stream
        }
        
    }
    
    
    private func add(asChildViewController viewController:UIViewController){
        
        //handle controller removal
        switch selectedVC {
        case .game:
            remove(asChildViewController: gameVC!)
        case .history:
            remove(asChildViewController: historyVC!)
        case .stream:
            remove(asChildViewController: streamVC!)
        }
        
        //add controller
        addChild(viewController)
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        viewHolder.addSubview(viewController.view)
        
        NSLayoutConstraint.activate([
            viewController.view.leadingAnchor.constraint(equalTo: viewHolder.leadingAnchor),
            viewController.view.trailingAnchor.constraint(equalTo: viewHolder.trailingAnchor),
            viewController.view.topAnchor.constraint(equalTo: viewHolder.topAnchor),
            viewController.view.bottomAnchor.constraint(equalTo: viewHolder.bottomAnchor)
        ])
        viewController.didMove(toParent: self)
    }
    
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
  
    
}
