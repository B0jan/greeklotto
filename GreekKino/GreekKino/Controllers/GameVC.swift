//
//  GameVC.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import Foundation
import UIKit




class GameVC:UIViewController{
    
    //    MARK: - Properties
    var kinoGame:KinoGame!
    
    private var selectedNumbers:[Int] = []{
        didSet{
            infoView.updateNumbersCount(with:selectedNumbers.count)
        }
    }
    
    //    MARK: - UIElements
    private lazy var scrollContainer:UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = .black
        return sv
    }()
    
    private lazy var widther:UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    private lazy var numbersCollectionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.isScrollEnabled = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    private lazy var textLabel:UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Odaberite 20 brojeva"
        label.textColor = .white
        label.backgroundColor = .clear
        label.font = UIFont.systemFont(ofSize: 15)
        label.sizeToFit()
        return label
    }()
    
    private var infoView:GameInfoView!
    private var oddsView:OddsView!
    
    //    MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        infoView.loadData(from: kinoGame)
    }
    
    
    //    MARK: - Actions
    
    
    //    MARK: - Protocol functions
    

    //    MARK: - Helpers
    func setupUI(){
        numbersCollectionView.delegate = self
        numbersCollectionView.dataSource = self
        numbersCollectionView.register(NumberCollectionViewCell.self, forCellWithReuseIdentifier: Constants.Cell.numCell)
        scrollContainer.showsVerticalScrollIndicator = false
        let wwd = widther.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width)
        infoView = GameInfoView()
        oddsView = OddsView()
        
        view.eAddSubViews(scrollContainer,infoView)
        scrollContainer.eAddSubViews(widther,oddsView,textLabel,numbersCollectionView)
        
        NSLayoutConstraint.activate([
            
            scrollContainer.topAnchor.constraint(equalTo: view.topAnchor),
            scrollContainer.bottomAnchor.constraint(equalTo: infoView.topAnchor),
            scrollContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            widther.leadingAnchor.constraint(equalTo: scrollContainer.leadingAnchor),
            widther.topAnchor.constraint(equalTo: scrollContainer.topAnchor),
            widther.trailingAnchor.constraint(equalTo: scrollContainer.trailingAnchor),
            widther.heightAnchor.constraint(equalToConstant: 1),
            wwd,
            
            oddsView.leadingAnchor.constraint(equalTo: scrollContainer.leadingAnchor),
            oddsView.topAnchor.constraint(equalTo: widther.bottomAnchor),
            oddsView.trailingAnchor.constraint(equalTo: scrollContainer.trailingAnchor),
            oddsView.heightAnchor.constraint(equalToConstant: 60),
            
            textLabel.topAnchor.constraint(equalTo: oddsView.bottomAnchor,constant: 5),
            textLabel.leadingAnchor.constraint(equalTo: scrollContainer.leadingAnchor,constant: 10),
            textLabel.trailingAnchor.constraint(equalTo: scrollContainer.trailingAnchor),
            
            
            numbersCollectionView.topAnchor.constraint(equalTo: textLabel.bottomAnchor,constant: 5),
            numbersCollectionView.leadingAnchor.constraint(equalTo: scrollContainer.leadingAnchor),
            numbersCollectionView.trailingAnchor.constraint(equalTo: scrollContainer.trailingAnchor),
            numbersCollectionView.bottomAnchor.constraint(equalTo: scrollContainer.bottomAnchor),
            numbersCollectionView.heightAnchor.constraint(equalToConstant: 1000),
            
            infoView.heightAnchor.constraint(equalToConstant: 70),
            infoView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            infoView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            infoView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    
  
}


extension GameVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 80
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cell.numCell, for: indexPath) as! NumberCollectionViewCell
        cell.configure(num:indexPath.item+1,isSelected: selectedNumbers.contains(indexPath.item+1))
        return cell
    }
}

extension GameVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,didSelectItemAt indexPath: IndexPath) {
        if selectedNumbers.contains(indexPath.item + 1){
            selectedNumbers.delete(object: indexPath.item + 1)
        } else {
            guard selectedNumbers.count < 20 else {
                infoView.alertNumber()
                return}
            selectedNumbers.append(indexPath.item+1)
        }
        
        let cell = collectionView.cellForItem(at: indexPath) as! NumberCollectionViewCell
        cell.setSelected()
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = Int(collectionView.frame.width / 5)
        let height = Int(collectionView.frame.height) / 16
        return CGSize(width: width, height: height)
    }
}


