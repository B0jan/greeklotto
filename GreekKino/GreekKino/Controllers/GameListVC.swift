//
//  ViewController.swift
//  GreekKino
//
//  Created by Administrator on 18/10/2020.
//

import UIKit


class GameListVC: UIViewController {
    
    
    //    MARK: - Properties
    var games:[KinoGame]?
    let reachability = Reachability()!
    
    enum ErrorText: String{
        case decodingProblem = "Something went wrong"
        case comunicationProblem = "Check your internet connection"
        case dataRemoved = "No more games to present"
    }
    
    var selectedIndex:IndexPath?
    //    MARK: - UIElements
    var tableView = UITableView()
    
    lazy var infoLabel:UILabel = {
        let lbl = UILabel()
        lbl.isHidden = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.sizeToFit()
        return lbl
    }()
    
    //    MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        notifyReachability()
        eActivityStartAnimating()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        removeRowsWhenReturning()
        guard let index = selectedIndex else {return}
        //needs to reload to set observer for timer again, because it was assigned on info screen on GameVC... if cell is stil presented
        tableView.reloadRows(at: [index], with: .none)
    }
    
    
    //    MARK: - Actions
    
    //    MARK: - Protocol functions
    
    //    MARK: - Helpers
    private func removeRowsWhenReturning(){
        var indexesToRemove:[IndexPath] = []
        var gamesToRemove:[KinoGame] = []
        var timesToRemove:[Int64] = []
        for (index,time) in TimeManager.expiredTimes.enumerated(){
            printInDebug(time)
            let gameToRemove = games?.first(where: { (game) -> Bool in
                return game.drawTime == time
            })
            //removes games on which time has expired
            if let game = gameToRemove{
                let indexToAppend = IndexPath(row: index, section: 0)
                if indexToAppend == selectedIndex{
                    selectedIndex = nil
                }
                indexesToRemove.append(indexToAppend)
                gamesToRemove.append(game)
                timesToRemove.append(time)
            }
        }
        
        guard !indexesToRemove.isEmpty else {return}
        for game in gamesToRemove{
            printInDebug(gamesToRemove)
            games?.delete(object: game)
        }
        tableView.beginUpdates()
        printInDebug(indexesToRemove)
        tableView.deleteRows(at: indexesToRemove, with: .fade)
        tableView.endUpdates()
        for time in timesToRemove{
            TimeManager.expiredTimes.delete(object: time)
        }
        
        if games!.isEmpty{
            presentLabelWith(text: .dataRemoved)
        }
        
    }
    
    private func setupUI(){
        let navView = NavigationItemView()
        navigationItem.titleView = navView
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        
        view.backgroundColor = .clear
        registerCell()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.eAddSubViews(tableView,infoLabel)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            infoLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            infoLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        
        
    }
    
    func registerCell(){
        tableView.register(GameCell.self, forCellReuseIdentifier: Constants.Cell.gameCell)
    }
    
    func loadData(){
        RequestHandler().getNewGames() {  [weak self](games, error) in
            guard let self = self else {return}
            self.eActivityStopAnimating()
            if let err = error {
                switch err {
                case .decodingFailure:
                    self.presentLabelWith(text: .decodingProblem)
                case .invalidURL:
                    self.presentLabelWith(text: .decodingProblem)
                case .requestFailed:
                    self.presentLabelWith(text: .comunicationProblem)
                }
                return
            }
            self.hideLabel()
            self.games = games
            self.tableView.reloadData()
        }
    }
    
    
    func presentLabelWith(text:ErrorText){
        eActivityStopAnimating()
        infoLabel.isHidden = false
        infoLabel.text = text.rawValue
    }
    
    func hideLabel(){
        infoLabel.isHidden = true
        infoLabel.text = ""
    }
    
    
    func notifyReachability(){
        reachability.whenReachable = {[unowned self] reachability in
            
            guard RequestHandler.requestsState == .failed else {return}
            if games == nil {
                hideLabel()
                eActivityStartAnimating()
                RequestHandler().getNewGames() {  [weak self](games, error) in
                    guard let self = self else {return}
                    self.eActivityStopAnimating()
                    if let err = error {
                        switch err {
                        case .decodingFailure:
                            self.presentLabelWith(text: .decodingProblem)
                        case .invalidURL:
                            self.presentLabelWith(text: .decodingProblem)
                        case .requestFailed:
                            self.presentLabelWith(text: .comunicationProblem)
                        }
                        return
                    }
                    eActivityStopAnimating()
                    self.games = games
                    self.tableView.reloadData()
                }
            }
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            printInDebug("Unable to start notifier")
        }
    }
    
    
    
}


extension GameListVC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath
        let vc = MainVC()
        vc.kinoGame = games![indexPath.row]
        ePushController(vc)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 20))
        headerView.backgroundColor = .black
        let leftLable = UILabel()
        leftLable.textColor = .white
        leftLable.frame = CGRect.init(x: 0, y: 5, width: 150, height: 20)
        leftLable.text = "Vreme izvlačenja"
        
        let rightLabel = UILabel()
        rightLabel.frame = CGRect.init(x: headerView.frame.width-150, y: 5, width: 150, height: 20)
        rightLabel.text = "Preostalo za uplatu"
        rightLabel.textColor = .white
        headerView.eAddSubViews(leftLable,rightLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension GameListVC:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return games != nil ? games!.count : 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.gameCell) as? GameCell{
            cell.configure(with: games![indexPath.row])
            return cell
        }
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.historyCell) as? PreviousGameCell{
            cell.configure(with: games![indexPath.row])
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    
}




