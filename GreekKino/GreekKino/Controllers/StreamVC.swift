//
//  StreamVC.swift
//  GreekKino
//
//  Created by Administrator on 19/10/2020.
//

import UIKit
import WebKit

class StreamVC: UIViewController, WKUIDelegate {

    //    MARK: - Properties
    private let keyPathToObserve:String = "estimatedProgress"
    private var progres:Float = 0.0
    
    
    //    MARK: - UIElements
    let spinner:UIActivityIndicatorView = {
        let spiner = UIActivityIndicatorView()
        spiner.color = .greekOldGoldDark
        return spiner
    }()
    
     var webView: WKWebView!
    
    
    //    MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.ignoresViewportScaleLimits = true
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
       setupUI()
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.webView.load(URLRequest(url: URL(string:  "https://ds.opap.gr/web_kino/kinoIframe.html?link=https://ds.opap.gr/web_kino/kino/html/Internet_PRODUCTION/KinoDraw_201910.html&resolution=1080x1920")!))
        spinner.startAnimating()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        printInDebug("MEMORY LEAK \(self)")
    }
    
    
    
     //    MARK: - Protocol functions
    
    ///WebView loading observer
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == keyPathToObserve {
            self.progres = Float(self.webView.estimatedProgress)
            if progres == 1 {
                spinner.stopAnimating()
            }
        }
    }
    
    
    //    MARK: - Helpers
    func setupUI(){
        webView.frame  = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 400)
        webView.contentMode = .scaleToFill
        self.webView.addObserver(self, forKeyPath: keyPathToObserve, options: .new, context: nil)
        view.eAddSubViews(webView,spinner)
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.topAnchor),
            webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

        ])
        
        guard let navBarHeight = navigationController?.navigationBar.frame.height else {return}
        spinner.layer.position.y = UIScreen.main.bounds.midY - navBarHeight
        spinner.layer.position.x = UIScreen.main.bounds.midX
        
        webView.isOpaque = false
        self.webView.scrollView.backgroundColor = .webViewBackgroundColor
    
        
    }
    
  
    
}
