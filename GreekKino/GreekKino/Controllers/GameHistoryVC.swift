//
//  GameHistoryVC.swift
//  GreekKino
//
//  Created by Administrator on 19/10/2020.
//

import UIKit


class GameHistoryVC: GameListVC {
    
    
    //    MARK: - Properties

    //    MARK: - UIElements
    
    
    //    MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = false
        notifyReachability()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    //    MARK: - Actions
    
    
    //    MARK: - Protocol functions
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 20))
        headerView.backgroundColor = .black
        let title = UILabel()
        title.textColor = .white
        title.frame = CGRect.init(x: tableView.frame.midX-75, y: 5, width: 150, height: 20)
        title.textAlignment = .center
        title.text = "Prethodna kola"
        headerView.addSubview(title)
        return headerView
    }
    
    
    
    //    MARK: - Helpers
    override func loadData() {
        
        
        RequestHandler().getOldGames() {  [weak self](games, error) in
            guard let self = self else {return}
            self.eActivityStopAnimating()
            if let err = error {
                switch err {
                case .decodingFailure:
                    self.presentLabelWith(text: .decodingProblem)
                case .invalidURL:
                    self.presentLabelWith(text: .decodingProblem)
                case .requestFailed:
                    self.presentLabelWith(text: .comunicationProblem)
                }
                return
            }
            
            self.games = games!.content?.filter({ (game) -> Bool in
                return game.winningNumbers != nil
            })
            self.tableView.reloadData()
        }
    }
    
    override func notifyReachability() {
        
        reachability.whenReachable = {[unowned self] reachability in
            guard RequestHandler.requestsState == .failed else {return}
            self.hideLabel()
            self.eActivityStartAnimating()
            if games == nil {
                RequestHandler().getOldGames() {  [weak self](games, error) in
                    guard let self = self else {return}
                    self.eActivityStopAnimating()
                    if let err = error {
                        switch err {
                        case .decodingFailure:
                            self.presentLabelWith(text: .decodingProblem)
                        case .invalidURL:
                            self.presentLabelWith(text: .decodingProblem)
                        case .requestFailed:
                            self.presentLabelWith(text: .comunicationProblem)
                        }
                        return
                    }
                    
                    self.games = games!.content?.filter({ (game) -> Bool in
                        return game.winningNumbers != nil
                    })
                    eActivityStopAnimating()
                    self.tableView.reloadData()
                }
            }
        }

        do {
            try reachability.startNotifier()
        } catch {
            printInDebug("Unable to start notifier")
        }
    }
    
    
    override func registerCell() {
        tableView.register(PreviousGameCell.self, forCellReuseIdentifier: Constants.Cell.historyCell)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

  
}
